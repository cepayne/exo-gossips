package com.nespresso.recruitment.gossip;

import java.util.HashMap;
import java.util.Map;

//import java.math.BigDecimal;

public class Gossips {
	
	private Map <String, Person> gossipers = new HashMap<>();
	Person fromPerson;
	Person toPerson;
	String words;

	public Gossips(String...names) {
		for (int i = 0; i < names.length; ++i){
			String name = names[i];
			gossipers.put(name, new Person(name));
		}
	}

	public Gossips from(String name) { 
		if (gossipers.containsKey(name)){
			fromPerson = gossipers.get(name);
		}
		return this;
	}
	
	public Gossips to(Gossips name) {
		if (gossipers.containsKey(name)){
			toPerson = gossipers.get(name);
			if (this.words != null)
				toPerson.setSay(words);
			if (fromPerson != null)
				toPerson.setFrom(fromPerson.getName());
		}
		return this;

}

	public Gossips say(String words) {
		this.words = words;
		return this;
	}

	public String ask(String name) {
		if(gossipers.containsKey(name)){
			toPerson = gossipers.get(name);
		}
		return null;
	}

	  public void spread() {
		int i=0;
		for (Person p : gossipers.values()){
			if (p == toPerson){
				p.setSay("");
				i++;
			}
			if (i==1) 
				break;
		}
		
	}
}