package com.nespresso.recruitment.gossip;

public class Person {

	private String to;
	private String from;
	private String say;
	private String name;
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSay() {
		return say;
	}
	public void setSay(String say) {
		this.say = say;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
